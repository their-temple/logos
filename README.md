# Their Temple Logos

Turning ideas into words and more, as [Logos](https://en.wikipedia.org/wiki/Logos) for [Their Temple](https://theirtemple.com)!

## Intentions

Storyboarding for Godot Scenes 

Originally intended as demo for logo and [artful intro](https://publicwords.com/2011/11/26/how-to-introduce-a-speaker-the-art-of-giving-and-receiving-a-great-introduction/) to [TheirTemple](https://theirtemple.com)

- Text-based, play and script writing focused
  - Starting from minimal modeling language, evolving towards natural language
- Terminal window for interactions
- Creates graph of Logos (Stories, Adventures and Epics...oh my!)
- Call-and-response support via dialogue extension and shared editing of scripts
- Verifies solution with [AStar](https://docs.godotengine.org/en/stable/classes/class_astar.html)
- Sense-focused (see, hear, touch)
- Desire-focused (hungry, lonely, wants)
- Feeling-focused (happy, sad, angry, hates)
- Relationship-focused (inside, next to, on top of, contains)
  - "Hanumanji CONTAINS Inventory"
- Action-focused (move, walk, kick, jump)
- [Subject-verb-object](https://en.wikipedia.org/wiki/Subject%E2%80%93verb%E2%80%93object) support (swing <to-be-swung>, shoot <to-be-shot>, eat <to-be-eaten>)
- Voice-over/translation support
  - "Hanumanji SAYS 'What are you doing?'
    - Automatic tr('What are you doing?')
    - res://Avatars/Hanumanji/Voice/what_are_you_doing.ogg
    - Automatic/Manual text to voice/sound mapping
- Hierarchy of search paths
  - "WHEN Hanumanji ENTERS SparkleMountain.Pond, Hanumanji HEARS LappingWaves" 
    - Searches SparkleMountain.Pond.LappingWaves & SparkleMountain.LappingWaves & ...
    - Might be any of ...
      - running LappingWaves Avatar under Pond (or containers)
      - scene res://Avatars/SparkleMountain/Sounds/LappingWaves.ogg
      - res://Avatars/LappingWaves.tscn
      - global res://Sounds/LappingWaves.wav
- Transformation-focused (made from, ...) - Jai Lord Vishnu!
  - "Tomato BECOMES Tomato Paste"
- Locked actions
  - "SparkleMountain.Entrance KEYED BY SparkleKey"
  - "* ENTERS SparkleMountain KEYED BY SparkleKey"
  - "Cheri Tomato TRANSFORMS Tomato INTO Tomato Paste KEYED BY Cheri Tomato"
- History-aware
  - "Tomato Paste BECAME_FROM Tomato"
- Conditional-focused (if-then, when)
  - "IF Greyheart FEELS sad, Greyheart SEEKS happy"
  - "IF Greyheart HEARS Green-Onion-Day-Record, Greyheart FEELS happy"
    - Search will find path from Greyheart FEELS sad, SEEKS happy <- FEELS happy <- HEARS Green-Onion-Day-Record
  - "WHEN [Greyheart, Cheri] INSIDE Eiffel Tower THEN IF [Cheri, Greyheart] TOUCH, [Cheri,Greyheart] FEEL divine"
  - "WHEN [Greyheart, Cheri] FEEL divine, THE_END"
- Wild-carding
  - "IF Greyheart EATS (* BECAME_FROM tomato), Greyheart FEELS happy"
- Integration with [Acro Avatars](https://gitlab.com/their-temple/acro-avatars)
  - A Model may have a Logos, describing what an Avatar can contain, how it can transform the world around it, what internal states it has
    - "Greyheart FEELS sad"
    - "Cheri CREATES Tomato"
    - "Cheri TRANSFORMS Tomato INTO Tomato Paste"
  - A View may have a Logos, describing what an Avatar looks like
    - "Greyheart COLOR <grey-red>" ... from grey to red depending on Tomato Products
    - "Cheri Tomato COLOR red"
    - "[Greyheart, Cheri Tomato] SIZE_OF Hanumanji.Hand"
    - "Hanumanji SMALLER_THAN SparkleMount"
- Automatic Glossary Generation with found path
  - Above examples would lead to Avatars(ACTION(state))
    - Inventory()
    - Pond()
    - SparkleMountain(CONTAINS(Pond))
    - Hanumanji(CONTAINS(Hanumanji/Inventory), SAYS(Hanumanji/Voice/what_are_you_doing), ENTERS(SparkleMountain.Pond), HEARS(SparkleMount.Pond.LappingWaves))
    - TomatoPaste()
    - Tomato(BECOMES(TomatoPaste))
    - EiffelTower()
    - CheriTomato(FEELS(divine), INSIDE(EiffelTower), TOUCH(Greyheart), GROWS(Tomato))
    - Greyheart(FEELS(sad,happy,divine), SEEKS(happy), HEARS(Greyheart/GreenOnionDayRecord), INSIDE(EiffelTower), TOUCH(CheriTomato), EATS(* TRANSFORMED_FROM Tomato))
- Two way changes and verification
- Integration with [Godot Visual Script](https://docs.godotengine.org/en/stable/getting_started/scripting/visual_script/getting_started.html)

NOTE: Examples come from ideas based on [Heart Today Gone Tomato](https://gitlab.com/heart-today/gone-tomato)

## Features



Godot Project and Scene for providing an 

## Demo

## Installation

## License

## Brainstorm

Starts whitespace, music
"Get to the Point."  "The Point is You."
"You are."  Point1 flashes into existence. 
"Are You?"/"You Are." Labels come flyig off randomly
en point, point, line, circle, all in 2D plane
Then spirals, while words fly and sounds dribble down the ears.


				Get to the Point.
				The Point is You.

                ...seek...for...

    I am.  									Am I?
    You are.  								Are You?
    We are. 								Are We?
    Are They?  								They are.
    Is God?  								God is.
    Is God Them?  							        God is Them.
    Are They God?  							        They are God.


				The Path from You to God.
				Their Temple Serves the Path.

				Will You Serve?



I am.		Point
You are.	Point
We are.	Line(I, You)
Are They?	Circle

God is The(m/y) are God.
(m/y)
(m)onth/(y)ear	~	12
(m)ile/(y)ard		~


Are We…?
Are We Content with Content?
